# daverona/mkdocs

[![pipeline status](https://gitlab.com/toscana/docker/mkdocs/badges/master/pipeline.svg)](https://gitlab.com/toscana/docker/mkdocs/commits/master)

* GitLab source repository: [https://gitlab.com/toscana/docker/mkdocs](https://gitlab.com/toscana/docker/mkdocs)
* Docker Hub repository: [https://hub.docker.com/r/daverona/mkdocs](https://hub.docker.com/r/daverona/mkdocs)

This is a Docker image of MkDocs on Alpine Linux.
This image provides:

* [MkDocs](https://www.mkdocs.org/) 1.0.4

## Installation

Install [Docker](https://hub.docker.com/search/?type=edition&offering=community) if you don't have one.
Then pull the image from Docker Hub repository:

```bash
docker image pull daverona/mkdocs
```

or build the image:

```bash
docker image build \
  --build-arg TIMEZONE=UTC \
  --tag daverona/mkdocs \
  .
```

When you build the image, you can specify a time zone of
Alpine Linux. If you don't specify the value of build argument
`TIMEZONE`, UTC will be used.

## Quick Start

Run the container:

```bash
docker container run --rm \
  daverona/mkdocs \
  mkdocs --version
```

It will show the version of MkDocs in the container.

## Usage

### Creating a Project

```bash
docker container run --rm \
  --volume /host/path/to/src:/var/local \
  daverona/mkdocs \
  mkdocs new .
```

This command creates under /host/path/to/src a documentation
project which consists of
a configuration file mkdocs.yml and a documentation directory docs where you keep all your Markdown files.

To configure your project (including theme changes), please read [https://www.mkdocs.org/user-guide/configuration/](https://www.mkdocs.org/user-guide/configuration/).
To learn how to organize and write your documentation, please read
[https://www.mkdocs.org/user-guide/writing-your-docs/](https://www.mkdocs.org/user-guide/writing-your-docs/).

### Serving a Project

```bash
docker container run --rm \
  --detach \
  --volume /host/path/to/src:/var/local \
  --publish 8000:8000 \
  --name mkdocs \
  daverona/mkdocs
```

This command serves docs directory under /host/path/to/src as a website. MkDocs will translate whatever changes you make to MarkDown files in real time and publish at [http://localhost:8000](http://localhost:8000).

### Building a Static Site

```bash
docker container run --rm \
  --volume /host/path/to/src:/var/local \
  daverona/mkdocs \
  mkdocs build
```

This command generates a static site (e.g. a collection of HTML, JavaScript and CSS files) from mkdocs.yml and docs directory under /host/path/to/src and places the output under /host/path/to/src/site. You can open index.html under it with a broswer to verify the output.

## References

* [https://www.mkdocs.org/](https://www.mkdocs.org/)
* [https://www.mkdocs.org/user-guide/configuration/](https://www.mkdocs.org/user-guide/configuration/)
* [https://github.com/mkdocs/mkdocs](https://github.com/mkdocs/mkdocs)
