FROM alpine:3.10

# Setting the system timezone
# @see https://wiki.alpinelinux.org/wiki/Setting_the_timezone
ARG TIMEZONE=UTC
RUN apk add --no-cache tzdata \
  && cp /usr/share/zoneinfo/$TIMEZONE /etc/localtime \
  && echo "$TIMEZONE" > /etc/timezone

# Installing MkDocs 1.0.4
ARG _VERSION=1.0.4
RUN apk add --no-cache python3 \
  && cd /usr/bin \
  && ln -s pip3 pip \
  && ln -s pydoc3 pydoc \
  && ln -s python3 python \
  && ln -s python3-config python-config \
  && pip install --no-cache-dir mkdocs==$_VERSION pandoc pygments

# Configuring ports and working directory
EXPOSE 8000/tcp
WORKDIR /var/local

CMD ["mkdocs", "serve", "--dev-addr=0.0.0.0:8000"]
